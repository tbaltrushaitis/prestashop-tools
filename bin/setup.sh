#!/usr/bin/env bash
##  -------------------------------------------------------------- ##
##  - Download, install and configure latest PrestaShop Engine
##  - Install php composer
##  -------------------------------------------------------------- ##

clear

##  -------------------------------------------------------------- ##
##  OPTIONS
##  -------------------------------------------------------------- ##
STORE_NAME="matusi"                     #   Target Store Name

PS_GIT_REPO="https://github.com/PrestaShop/PrestaShop.git"
PS_GIT_DIR="PrestaShop.git"

WEB_USER="apache"                       #   Group of webserver used on host
DIR_WEB="webroot"                       #   Store Webroot Directory
DIR_ROOT="$(pwd)"                       #   Full path to target directory

DOMAIN="${STORE_NAME}.gsm-center.com.ua"

DIR_INSTALL="install-dev"
STORE_DIR="${DIR_ROOT}/${STORE_NAME}"
STORE_WEB="${STORE_DIR}/${DIR_WEB}"
STORE_LOG="${STORE_DIR}/${STORE_NAME}.log"

##  -------------------------------------------------------------- ##
##  DATABASE CONNECTION
##  -------------------------------------------------------------- ##
DB_HOST="localhost"
DB_NAME="${STORE_NAME}_db"
DB_USER="${STORE_NAME}_user"


##  -------------------------------------------------------------- ##
##  PRE-EXECUTE
##  -------------------------------------------------------------- ##

DATE="$(date +"%Y%m%d%H%M%S")"
DATETIME="$(date "+%Y-%m-%d")_$(date "+%H-%M-%S")"
BACK_PATH="/opt/backup/${DB_NAME}/"
# mkdir -p ${STORE_WEB} &2>/dev/null
# chown root:"${WEB_USER}" "${STORE_WEB}"
# chmod 775 "${STORE_WEB}"

DB_PASS=`pwgen -s1 16`
echo "\n\n\n\nDATETIME:\t${DATETIME}" > "${STORE_LOG}"
echo "DB_PASS:\t${DB_PASS}" > "${STORE_LOG}"


printf "\n------------------------------  ${DATE}  ------------------------------n";

##  -------------------------------------------------------------- ##
##  DATABASE BACKUP
##  -------------------------------------------------------------- ##
function db_backup {
    printf "\n-------------------------  BACKUP DATABASE and TABLES  -------------------------------\n";
    mkdir -p ${BACK_PATH}

    printf "\n=============================\n";
    printf "BACKUP OF FULL DATABASE ${DB_NAME} ... ";
    mysqldump ${DB_NAME} > "${BACK_PATH}${DATE}.${DB_NAME}.sql"
    printf "DONE\n";
    printf "=============================\n";

    printf "\nPER TABLE BACKUP: \n";
    printf "=============================\n";
    for TABLE in $(mysql -e "USE ${DB_NAME}; SHOW TABLES;" | grep -v "Tables" | sort)
        do
            printf "${TABLE} ... ";
            mysqldump ${DB_NAME} ${TABLE} > "${BACK_PATH}${DATE}.${DB_NAME}.${TABLE}.sql"
            printf "DONE\n";
        done
    printf "======================================================\n";
    printf "FINISHED: PER TABLE BACKUP\n\n";
}


##  -------------------------------------------------------------- ##
##  CREATE DATABASE AND USER
##  -------------------------------------------------------------- ##
function db_create {
    printf "\n  -------------------------  CREATE DATABASE AND USER --------------------------\n";
    mysql -e "DROP USER '${DB_USER}'@'${DB_HOST}';"
    mysql -e "CREATE USER '${DB_USER}'@'${DB_HOST}' IDENTIFIED BY '${DB_PASS}';"
    mysql -e "DROP DATABASE IF EXISTS ${DB_NAME};"
    mysql -e "CREATE DATABASE ${DB_NAME};"
    mysql -e "GRANT ALL PRIVILEGES ON ${DB_NAME}.* TO '${DB_USER}'@'${DB_HOST}' IDENTIFIED BY '${DB_PASS}' WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;"
    mysql -e "FLUSH PRIVILEGES;"
    mysql -e "SHOW GRANTS FOR ${DB_USER}@${DB_HOST};"

    printf "USER ${DB_USER}@${DB_HOST} and DATABASE ${DB_NAME} RECREATED \n\n";
    printf "======================================================\n";
}


##  -------------------------------------------------------------- ##
##  CLONE AND DEPLOY GIT REPO
##  -------------------------------------------------------------- ##
function git_tasks {
    printf "\n======================================================\n";
    printf "GIT REPO: ${GIT_REPO} \n\n";

    cd "${STORE_DIR}"
    git clone "${GIT_REPO}" "${GIT_DIR}"
    printf "DONE\n";

    # cd "${STORE_NAME}"
    cd "${GIT_DIR}"
    git checkout -l master
    git pull origin master
}

##  -------------------------------------------------------------- ##
##  PERMISSIONS
##  -------------------------------------------------------------- ##
function set_permissions {
    printf "\n------------------  SET PERMISSIONS  ------------------------\n";
    chown -R root:${WEB_GROUP} "${STORE_DIR}"
    chmod 775 "${STORE_DIR}"

    cd "${STORE_DIR}"
    find . -type d -exec chmod 775 {} \;
    find . -type f -exec chmod 664 {} \;

    printf "PERMISSIONS CHANGED \n\n";
    printf "======================================================\n";
}


##  ------------------------------------------ INSTALL AND CONFIGURE DATABASE ---------------------------------  ##

##  -------------------------------------------------------------- ##
##  PRESTASHOP
##  -------------------------------------------------------------- ##
function setup_shop {
    printf "\n=====================================================================================================\n";
    printf "DOMAIN: ${DOMAIN} \n\n";
    cd "${DIR_DEPLOY}/${STORE_NAME}"
    printf "PWD: $(pwd)";
    printf "\n";
    #exit

    EXEC_INSTALL="php \
        ${DIR_INSTALL}/index_cli.php    \
        --step=all  \
        --language=ru   \
        --all_languages=0   \
        --timezone=Europe/Kiev  \
        --base_uri=/    \
        --db_clear=1    \
        --db_create=1   \
        --domain=${STORE_NAME}  \
        --db_server=${DB_HOST}  \
        --db_name=${DB_NAME}    \
        --db_user=${DB_USER}    \
        --db_password=${DB_PASS}    \
        --prefix=   \
        --engine=InnoDB \
        --name=PrestaShop Store   \
        --activity=0    \
        --country=ua    \
        --firstname=OwnerFirst   \
        --lastname=OwnarLast \
        --password=very_secret \
        --email=some.user@gmail.com \
        --license=0 \
        --newsletter=0  \
        --send_email=0  \
    "

    printf "\n EXECUTE [${EXEC_INSTALL}]:\n"
    ${EXEC_INSTALL}
    printf "EXECUTE DONE\n";

}


##  -------------------------------------------------------------- ##
##  COMPOSER
##  -------------------------------------------------------------- ##
function composer_setup {
    SIGNATURE_EXPECTED=$(wget http://composer.github.io/installer.sig -O - -q)
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    SIGNATURE_ACTUAL=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

    if [ "$SIGNATURE_EXPECTED" = "$SIGNATURE_ACTUAL" ]
    then
        printf "OK: Correct installer signature [$SIGNATURE_ACTUAL]\n"
        php composer-setup.php --install-dir=/bin --filename=composer
        RESULT=$?
        mv composer-setup.php composer-setup-DONE-$(date +"%s").php
        printf "COMPOSER INSTALL FINISHED\n"
        exit $RESULT
    else
        >&2 printf 'ERROR: Invalid installer signature\n'
        mv composer-setup.php composer-setup-INVALID-$(date +"%s").php
        exit 1
    fi
}


##  --------------------------------------------------------------------------------------------- //
##  #  EXECUTION
##  --------------------------------------------------------------------------------------------- //

#composer_setup
db_backup
db_create
#git_tasks
#set_permissions
setup_shop
